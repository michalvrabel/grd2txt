# Installation

On Ubuntu/Debian:
```
bash install.sh
```

There might be problems with `requirements.txt`, if so, please let me know.

On other systems, please install following packages:
```
python3  
virtualenv
```

Python required packages are following (normally will be installed automatically using `requirements.txt`, everything else will be installed as a dependency).
```
steno3d-surfer   
numpy   
matplotlib
```

Library *matplotlib* is not necessary if you don't want to visualize data with `grd2vis.py` program.

# Usage

For conversion use `grd2txt.py` program, which you can run also using bash script `grd2txt.sh`. 
```
bash grd2txt.sh file [file ...]
```

Accepted extensions are `.grd`, `.ssh`, `.sshmax`, `.time`.

For quick visualization use `grd2vis.py`, which can be run also by using bash script `grd2vis.sh`.
```
bash grd2vis.sh [-h] [-l LOG_SCALE] [-c COLORMAP] files [files ...]
```
**positional arguments**:

- `files` - List of files to animate

**optional arguments**:

- `-h`, `--help`  -  show this help message and exit
- `-l LOG_SCALE`, `--log-scale LOG_SCALE`  -  Use log scale. If this option is true, z axis limits are not set.
- `-c COLORMAP`, `--colormap COLORMAP`  -  For available colormaps see: https://matplotlib.org/examples/color/colormaps_reference.html

To play animation you can run `grd2vis.py` by following command:
```
./grd2vis.sh -l y  ../easywave/run/eWave.2D.*.ssh
```

Files will be played in order of arguments. Using `*` should provide correct order if files are properly named. `-l y` says that the logaritic scale should be used.

Parameters of the each file are written to the *stdout*. Converted files are saved inside current working directory with same name as the original, but with added extension `.data.txt`.

If you want to directly run `grd2txt.py` or `grd2vis.py`, you need to have configured enviroment (locally configured via the virtual enviroment). This can be done by `setup_env.sh` script, which activates virtual enviroment inside `venv` directory.

