#!/usr/bin/python3
import sys
import numpy as np
import os
import steno3d_surfer
import argparse
from PIL import Image
# import matplotlib as mpl
# mpl.use('Agg')
# import matplotlib.pyplot as plt
# import matplotlib.animation as animation
# import matplotlib.colors as colors
from struct import unpack

import steno3d
import steno3d_surfer.parser


class ssh_grd(steno3d_surfer.grd):
    extensions = ('grd', 'ssh', 'sshmax', 'time')

    def parse(self, project=None, verbose=True, as_topo=True):
        super().parse(self, project, verbose, as_topo)

    def _surfer6bin(self, verbose, warnings):
        with open(self.file_name, 'rb') as f:
            if unpack('4s', f.read(4))[0] != b'DSBB':
                raise steno3d.parsers.ParseError(
                    'Invalid file identifier for Surfer 6 binary .grd '
                    'file. First 4 characters must be DSBB.'
                )
            # nx = unpack('<h', f.read(2))[0]
            # ny = unpack('<h', f.read(2))[0]
            nx = unpack('<H', f.read(2))[0]  # changed signed short (<h) to unsigned short (<h)
            ny = unpack('<H', f.read(2))[0]
            xlo = unpack('<d', f.read(8))[0]
            xhi = unpack('<d', f.read(8))[0]
            ylo = unpack('<d', f.read(8))[0]
            yhi = unpack('<d', f.read(8))[0]
            zlo = unpack('<d', f.read(8))[0]
            zhi = unpack('<d', f.read(8))[0]
            data = np.ones(nx * ny)
            for i in range(nx * ny):
                zdata = unpack('<f', f.read(4))[0]
                if zdata >= 1.701410009187828e+38:
                    data[i] = np.nan
                else:
                    data[i] = zdata

        grd_info = steno3d_surfer.parser.GridInfo(
            ncol=nx,
            nrow=ny,
            xll=xlo,
            yll=ylo,
            xsize=(xhi - xlo) / (nx - 1),
            ysize=(yhi - ylo) / (ny - 1),
            zmin=zlo,
            zmax=zhi,
            data=data.reshape(nx, ny, order='F')
        )

        return grd_info

    def _surfer6ascii(self, verbose, warnings):
        with open(self.file_name, 'r') as f:
            if f.readline().strip() != 'DSAA':
                raise steno3d.parsers.ParseError(
                    'Invalid file identifier for Surfer 6 ASCII .grd '
                    'file. First line must be DSAA'
                )
            [ncol, nrow] = [int(n) for n in f.readline().split()]
            [xmin, xmax] = [float(n) for n in f.readline().split()]
            [ymin, ymax] = [float(n) for n in f.readline().split()]
            [zmin, zmax] = [float(n) for n in f.readline().split()]
            data = np.zeros((nrow, ncol))
            val_buffer = []
            for i in range(nrow):
                while len(val_buffer) < ncol:
                    val_buffer.extend([float(n) for n in f.readline().split()])
                data[i, :] = val_buffer[:ncol]
                del val_buffer[:ncol]
            if len(val_buffer) > 0:
                raise steno3d.parsers.ParseError("Value buffer still contains values but all rows has been read")
            data = data.T

        grd_info = steno3d_surfer.parser.GridInfo(
            ncol=ncol,
            nrow=nrow,
            xll=xmin,
            yll=ymin,
            xsize=(xmax-xmin)/(ncol-1),
            ysize=(ymax-ymin)/(nrow-1),
            zmin=zmin,
            zmax=zmax,
            data=data
        )

        return grd_info

    @staticmethod
    def _warn(warning, warnings, verbose):
        if warning in warnings:
            return
        warnings.add(warning)
        if verbose:
            print('  ' + warning)



def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    if v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


parser = argparse.ArgumentParser(description='Visualize grid files in argv.')
parser.add_argument('files', nargs='+', help='List of files to animate')
# parser.add_argument('-l', '--log-scale', type=bool, default=False, help='Use log scale. If this option is true, z axis limits are not set.')
parser.add_argument('-l', "--log-scale", type=str2bool, default=False,
                    help="If this option is true, z axis limits are not set.")
parser.add_argument('-i', '--interval', type=int, default=500, help="Interval between animation steps.")
parser.add_argument('-o', '--out-file', default=None, help="Output file, be default {original_name}.png")
parser.add_argument('-c', '--colormap', default="nipy_spectral",
                    help="For available colormaps see: https://matplotlib.org/examples/color/colormaps_reference.html")

args = parser.parse_args()

print("Processed files: ")
print(", ".join(args.files))


def load_file(infile_path):
    if not os.path.exists(infile_path):
        sys.stderr.write("File {} does not exist\n".format(infile_path))
        sys.exit(5)

    grd = ssh_grd(infile_path)
    gridinfo = grd.extract()

    # infile_name = os.path.basename(infile_path)

    # outfile_data = "{}.data.txt".format(infile_name)
    # np.savetxt(outfile_data, gridinfo.data, delimiter="\t")

    atts = ["ncol", "nrow", "xll", "yll", "xsize", "ysize", "zmin", "zmax"]

    print("{}:".format(infile_path))
    for att in atts:
        print("{}:\t{}".format(att, getattr(gridinfo, att)))
    # print("data_file:\t{}".format(outfile_data))
    print("")

    return gridinfo

for infile_path in args.files:
    print("Loading GRD")

    gridinfo = load_file(infile_path)

    print("GRD {} loaded".format(infile_path))

    # fig1 = plt.figure()
    # im = plt.imshow(np.rot90(gridinfo.data), norm=colors.LogNorm() if args.log_scale else None, animated=True,
    #                 cmap=plt.get_cmap(args.colormap), interpolation="nearest",
    #                 extent=[gridinfo.xll, gridinfo.xll + gridinfo.xsize * gridinfo.ncol, gridinfo.yll,
    #                         gridinfo.yll + gridinfo.ysize * gridinfo.nrow])
    #
    # if not args.log_scale:
    #     im.set_clim(vmin=gridinfo.zmin, vmax=gridinfo.zmax)
    #
    # plt.colorbar()

    # vmin=0.01, vmax=1

    print("Normalizing data ...")
    max = gridinfo.data.max()
    min = gridinfo.data.min()

    im_data = np.rot90( np.uint8( ( (gridinfo.data - min) / (max - min) )*255 ))

    print("Creating image ...")
    im = Image.fromarray(np.uint8(im_data))

    i = 0;

    infile_name = os.path.basename(infile_path)

    if not args.out_file is None and len(args.out_file) > 0:
        outfile_path = args.out_file
    else:
        outfile_path = os.path.join(".","{}.png".format(infile_name))

    im.save(outfile_path)

    # fig1.savefig(outfile_path)
    #
    # fig1.clear()