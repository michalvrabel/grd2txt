#!/bin/bash
# potrebne kniznice: 
#   python3  virtualenv
# kniznice v pythone:
#   requirements.txt (verzie mozno nie su podstatne)

# directory of the script (https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within) 
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

if [ -f /usr/bin/apt-get ]; then
    sudo /usr/bin/apt-get install python3
    sudo /usr/bin/apt-get install virtualenv
else
    echo -e "Program apt-get is not available.\nPlease install following: python3  virtualenv" >&2
fi    

if [ -f /usr/bin/virtualenv ]; then
    /usr/bin/virtualenv -p python3 "$DIR/venv"

    source "$DIR/venv/bin/activate"
    pip install -r "$DIR/requirements.txt"
else
    echo -e "Program virtualenv is not available.\nPlease setup enviroment manually. See $DIR/requirements.txt" >&2
fi    