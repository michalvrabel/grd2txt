#!/bin/bash

# directory of the script (https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within) 
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

if [ ! -d "$DIR/venv" ]; then
    echo "Missing venv directory. Please run install.sh" >&2
    exit 1
fi

source "$DIR/venv/bin/activate"
python "$DIR/grd2xyz.py" "${@:1}"
