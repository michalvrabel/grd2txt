#!/usr/bin/python3
import sys
import steno3d_surfer
import numpy as np
import os
from struct import unpack
import steno3d.parsers

class ssh_grd(steno3d_surfer.grd):
    extensions = ('grd','ssh','sshmax','time')
    
    def parse(self, project=None, verbose=True, as_topo=True):
        super().parse(self, project, verbose, as_topo)

    def _surfer6bin(self, verbose, warnings):
        with open(self.file_name, 'rb') as f:
            if unpack('4s', f.read(4))[0] != b'DSBB':
                raise steno3d.parsers.ParseError(
                    'Invalid file identifier for Surfer 6 binary .grd '
                    'file. First 4 characters must be DSBB.'
                )
            # nx = unpack('<h', f.read(2))[0]
            # ny = unpack('<h', f.read(2))[0]
            nx = unpack('<H', f.read(2))[0]  # changed signed short (<h) to unsigned short (<h)
            ny = unpack('<H', f.read(2))[0]
            xlo = unpack('<d', f.read(8))[0]
            xhi = unpack('<d', f.read(8))[0]
            ylo = unpack('<d', f.read(8))[0]
            yhi = unpack('<d', f.read(8))[0]
            zlo = unpack('<d', f.read(8))[0]
            zhi = unpack('<d', f.read(8))[0]
            data = np.ones(nx * ny)
            for i in range(nx * ny):
                zdata = unpack('<f', f.read(4))[0]
                if zdata >= 1.701410009187828e+38:
                    data[i] = np.nan
                else:
                    data[i] = zdata

        grd_info = steno3d_surfer.parser.GridInfo(
            ncol=nx,
            nrow=ny,
            xll=xlo,
            yll=ylo,
            xsize=(xhi - xlo) / (nx - 1),
            ysize=(yhi - ylo) / (ny - 1),
            zmin=zlo,
            zmax=zhi,
            data=data.reshape(nx, ny, order='F')
        )

        return grd_info

if len(sys.argv) < 2:
	sys.stderr.write("missing file path")
	sys.exit(1)

for i in range(1, len(sys.argv)):
                   
    infile_path = sys.argv[i]

    if not os.path.exists(infile_path):
        sys.stderr.write("File {} does not exist\n".format(infile_path))
        sys.exit(5)

    grd = ssh_grd(infile_path)
    gridinfo = grd.extract()

    infile_name = os.path.basename(infile_path)
    
    outfile_data = "{}.data.txt".format(infile_name)
    np.savetxt(outfile_data, gridinfo.data, delimiter="\t")

    atts = ["ncol", "nrow", "xll", "yll", "xsize", "ysize", "zmin", "zmax"]

    print("{}:".format(infile_path))
    for att in atts:
        print("{}:\t{}".format(att,getattr(gridinfo, att)))

    print("data_file:\t{}".format(outfile_data))
    print("")
    


# vizualizacia

#import matplotlib.pyplot as plt
#plt.imshow(gridinfo.data)
#plt.show()

