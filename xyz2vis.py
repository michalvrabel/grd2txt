#!/usr/bin/python3
import sys
import numpy as np
import os
import argparse
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.colors as colors
from numpy import genfromtxt

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    if v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


parser = argparse.ArgumentParser(description='Visualize grid files in argv.')
parser.add_argument('files', nargs='+', help='List of files to animate')
# parser.add_argument('-l', '--log-scale', type=bool, default=False, help='Use log scale. If this option is true, z axis limits are not set.')
parser.add_argument('-l', "--log-scale", type=str2bool, default=False,
                    help="If this option is true, z axis limits are not set.")
parser.add_argument('-p',"--preload", type=str2bool, default=False, help="Files are preloaded before animation.")
parser.add_argument('-c', '--colormap', default="nipy_spectral",
                    help="For available colormaps see: https://matplotlib.org/examples/color/colormaps_reference.html")
parser.add_argument('-d', '--delimiter', default=None, help="Delimiter")
parser.add_argument('-i', '--interval', type=int, default=500, help="Interval between animation steps.")
parser.add_argument('--vmin', type=int, default=None, help="Z value minimum")
parser.add_argument('--vmax', type=int, default=None, help="Z value maximum")

args = parser.parse_args()

print("Animated files: ")
print(", ".join(args.files))

if len(sys.argv) < 2:
    sys.stderr.write("missing file path")
    sys.exit(1)


def load_file(infile_path):

    if not os.path.exists(infile_path):
        sys.stderr.write("File {} does not exist\n".format(infile_path))
        sys.exit(5)

    #data = genfromtxt(infile_path, delimiter=args.delimiter)  #
    # return data

    dc1, dc2, dc3 = np.loadtxt(infile_path, delimiter=args.delimiter, unpack=True)

    # print(dc1)
    # print(dc2)
    # print(dc3)

    u_dc1 = np.unique(dc1)
    u_dc2 = np.unique(dc2)

    X, Y = np.meshgrid(u_dc2, u_dc1)
    C = dc3.reshape(len(u_dc1),len(u_dc2))

    vmin = None
    if not args.log_scale and args.vmin is None:
        vmin = dc3.min()
    elif not args.vmin is None:
        vmin = args.vmin

    vmax = None
    if not args.log_scale and args.vmax is None:
        vmax = dc3.max()
    elif not args.vmax is None:
        vmax = args.vmax

    xmin = u_dc1.min()
    xmax = u_dc1.max()
    ymin = u_dc2.min()
    ymax = u_dc2.max()

    return X, Y, C, vmin, vmax, xmin, xmax, ymin, ymax


preloaded = []

X = Y = C = vmin = vmax = None

if args.preload:
    print("Please wait preloading...")
    for infile_path in args.files:
        preloaded.append(load_file(infile_path))
    print("All files loaded")
    X, Y, C, vmin, vmax, xmin, xmax, ymin, ymax = preloaded[0]
else:
    X, Y, C, vmin, vmax, xmin, xmax, ymin, ymax = load_file(args.files[0])



fig1 = plt.figure()

plt.axis("equal")

im = plt.pcolormesh(Y, X, C, norm=colors.LogNorm() if args.log_scale else None,
                    cmap=plt.get_cmap(args.colormap),
                    vmin=vmin, vmax=vmax
                    )
#im.set_extent([xmin, xmax, ymin, ymax])
plt.colorbar()

i = 0;


def animation_init():
    global i
    i = 0;


def animation_update(num):
    global i
    global im
    global args
    global preloaded
    if i + 1 >= len(args.files):
        i = 0
    else:
        i += 1

    if args.preload and len(preloaded) > i:
        X, Y, C, vmin, vmax, xmin, xmax, ymin, ymax = preloaded[i]
    else:
        X, Y, C, vmin, vmax, xmin, xmax, ymin, ymax = load_file(args.files[i])

    fig1.clear()

    plt.axis("equal")

    im = plt.pcolormesh(Y, X, C, norm=colors.LogNorm() if args.log_scale else None,
                        cmap=plt.get_cmap(args.colormap),
                        vmin=vmin, vmax=vmax
                        )
    plt.colorbar()


    #im.set_extent([xmin, xmax, ymin, ymax])
    # if not args.log_scale:
    #     im.set_clim(vmin=gridinfo.zmin, vmax=gridinfo.zmax)


if len(args.files) > 2:
    line_ani = animation.FuncAnimation(fig1, animation_update, len(args.files), animation_init, interval=args.interval)
    # , blit=True, repeat=True

plt.show()

