#!/usr/bin/python3
import sys
import steno3d_surfer
import numpy as np
import os
import argparse
from scipy.io import netcdf
from struct import unpack, pack
from tqdm import tqdm
class ssh_grd(steno3d_surfer.grd):
    extensions = ('grd', 'ssh', 'sshmax', 'time')

    def parse(self, project=None, verbose=True, as_topo=True):
        super().parse(self, project, verbose, as_topo)

def _surfer6bin(self, verbose, warnings):
        with open(self.file_name, 'rb') as f:
            if unpack('4s', f.read(4))[0] != b'DSBB':
                raise Exception(
                    'Invalid file identifier for Surfer 6 binary .grd '
                    'file. First 4 characters must be DSBB.'
                )
            nx = unpack('<h', f.read(2))[0]
            ny = unpack('<h', f.read(2))[0]
            xlo = unpack('<d', f.read(8))[0]
            xhi = unpack('<d', f.read(8))[0]
            ylo = unpack('<d', f.read(8))[0]
            yhi = unpack('<d', f.read(8))[0]
            zlo = unpack('<d', f.read(8))[0]
            zhi = unpack('<d', f.read(8))[0]
            data = np.ones(nx * ny)
            for i in range(nx * ny):
                zdata = unpack('<f', f.read(4))[0]
                if zdata >= 1.701410009187828e+38:
                    data[i] = np.nan
                else:
                    data[i] = zdata

        grd_info = steno3d_surfer.parser.GridInfo(
            ncol=nx,
            nrow=ny,
            xll=xlo,
            yll=ylo,
            xsize=(xhi-xlo)/(nx-1),
            ysize=(yhi-ylo)/(ny-1),
            zmin=zlo,
            zmax=zhi,
            data=data.reshape(nx, ny, order='F')
        )

        return grd_info


parser = argparse.ArgumentParser(description='Convert grid files to xyz files')
parser.add_argument('files', nargs='+', help='List of files to convert')
parser.add_argument('--elevation', default="elevation", help="Elevation variable name")
parser.add_argument('--lat', default="lat", help="Latitude variable name")
parser.add_argument('--lon', default="lon", help="Longitude variable name")
parser.add_argument('-t','--type', default="DSBB", help="Output grid file type, either DSBB (binary) or DSAA (text)")
# parser.add_argument('-x', '--x-format', default="{:.18e}", help="x column format (see python format options - https://pyformat.info/)")
# parser.add_argument('-y', '--y-format', default="{:.18e}", help="y column format (see python format options - https://pyformat.info/)")
parser.add_argument('-z', '--z-format', default="{:.8f}", help="z column format (see python format options - https://pyformat.info/)")
# parser.add_argument('-o', '--data-output', default="stdout", help="destination of data output, possible values: stdout, stderr, file")
# parser.add_argument('-i', '--info-output', default="stderr", help="destination of data output, possible values: stdout, stderr, file")
parser.add_argument('-d', '--dest-dir', default=".", help="Destination directory")

args = parser.parse_args()

for infile_path in args.files:

    if not os.path.exists(infile_path):
        sys.stderr.write("File {} does not exist\n".format(infile_path))
        sys.exit(5)

    f = netcdf.netcdf_file(infile_path, 'r') #'/media/vrabel/Elements/GEBCO_2014_2D.nc'

    attrs = {
        "file": infile_path,
        "netcdf.dimensions": [],
        "netcdf.variables": []
    }

    for dim_name, dim_size in f.dimensions.items():
        attrs["netcdf.dimensions"].append("{} ({})".format(dim_name, dim_size))

    for var_name, var_obj in f.variables.items():
        attrs["netcdf.variables"].append("{} ({})".format(var_name, len(var_obj.data)))

    print(attrs)

    var_lat = f.variables[args.lat]
    var_lat_len = len(var_lat.data)
    val_diff_lat_str = "{:.5f}".format(var_lat[1] - var_lat[0])

    for i in range(2, var_lat_len):
        if "{:.5f}".format(var_lat[i] - var_lat[i - 1]) != val_diff_lat_str:
            sys.stderr.write("Latitude values are not evenly spaced\n")
            sys.exit(2)
            break

    var_lon = f.variables[args.lon]
    var_lon_len = len(var_lon.data)
    val_diff_lon_str = "{:.5f}".format(var_lon[1] - var_lon[0])

    for i in range(2, var_lon_len):
        if "{:.5f}".format(var_lon[i] - var_lon[i - 1]) != val_diff_lon_str:
            sys.stderr.write("Longitude values are not evenly spaced\n")
            sys.exit(2)
            break

    nx = var_lon_len
    ny = var_lat_len
    xlo = var_lon[0]
    xhi = var_lon[-1]
    ylo = var_lat[0]
    yhi = var_lat[-1]

    if not args.elevation in f.variables:
        sys.stderr.write("Elevation variable is not found\n")
        sys.exit(2)

    np_elevation = np.array(f.variables[args.elevation].data)
    np_elevation_t = np_elevation.transpose()                   # should be like steno3d_surfer.parser.GridInfo.data

    zlo = np_elevation.max()
    zhi = np_elevation.min()

    elevation_flattened = np_elevation_t.flatten('F') # if C had been used, transpose would have been unnecessary PROBABLY

    # writing to outfile

    infile_name = os.path.basename(infile_path)
    outfile_name = "{}.grd".format(infile_name)
    outfile_path = os.path.join(args.dest_dir, outfile_name)

    if args.type == 'DSBB':
        with open(outfile_path,"wb") as ofb:
            ofb.write(pack("4s", b'DSBB'))
            ofb.write(pack("<H",nx))  # warning originally it should be SIGNED (<h); this is UNSIGNED; but easywave reads unsigned short
            ofb.write(pack("<H",ny))
            ofb.write(pack("<d",xlo))
            ofb.write(pack("<d",xhi))
            ofb.write(pack("<d",ylo))
            ofb.write(pack("<d",yhi))
            ofb.write(pack("<d",zlo))
            ofb.write(pack("<d",zhi))
            for v in tqdm(elevation_flattened):
                ofb.write(pack("<f",v))
    else:
        with open(outfile_path,"w") as oft:
            oft.write("DSAA\n")
            oft.write("{} {}\n".format(nx,ny))
            oft.write("{} {}\n".format(xlo,xhi))
            oft.write("{} {}\n".format(ylo,yhi))
            oft.write("{} {}\n".format(zlo,zhi))
            for row in tqdm(np_elevation):
                oft.write(" ".join([args.z_format.format(c) for c in row]))
                oft.write("\n")



    print("DONE")


#unnpack('4s', f.read(4))[0] != b'DSBB'
# nx = unpack('<h', f.read(2))[0]
# ny = unpack('<h', f.read(2))[0]
# xlo = unpack('<d', f.read(8))[0]
# xhi = unpack('<d', f.read(8))[0]
# ylo = unpack('<d', f.read(8))[0]
# yhi = unpack('<d', f.read(8))[0]
# zlo = unpack('<d', f.read(8))[0]
# zhi = unpack('<d', f.read(8))[0]
# for i in range(nx * ny):
#     zdata = unpack('<f', f.read(4))[0]


#if f.readline().strip() != 'DSAA'
# [ncol, nrow] = [int(n) for n in f.readline().split()]
# [xmin, xmax] = [float(n) for n in f.readline().split()]
# [ymin, ymax] = [float(n) for n in f.readline().split()]
# [zmin, zmax] = [float(n) for n in f.readline().split()]
# data = np.zeros((nrow, ncol))
# for i in range(nrow):
#     data[i, :] = [float(n) for n in f.readline().split()]
# data = data.T