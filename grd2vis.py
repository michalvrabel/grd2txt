#!/usr/bin/python3
import sys
import numpy as np
import os
import steno3d_surfer
import argparse
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.colors as colors
from struct import unpack

import steno3d
import steno3d_surfer.parser

class ssh_grd(steno3d_surfer.grd):
    extensions = ('grd','ssh','sshmax','time')
    
    def parse(self, project=None, verbose=True, as_topo=True):
        super().parse(self, project, verbose, as_topo)
        
    def _surfer6bin(self, verbose, warnings):
        with open(self.file_name, 'rb') as f:
            if unpack('4s', f.read(4))[0] != b'DSBB':
                raise steno3d.parsers.ParseError(
                    'Invalid file identifier for Surfer 6 binary .grd '
                    'file. First 4 characters must be DSBB.'
                )
            # nx = unpack('<h', f.read(2))[0]
            # ny = unpack('<h', f.read(2))[0]
            nx = unpack('<H', f.read(2))[0]     #changed signed short (<h) to unsigned short (<h)
            ny = unpack('<H', f.read(2))[0]
            xlo = unpack('<d', f.read(8))[0]
            xhi = unpack('<d', f.read(8))[0]
            ylo = unpack('<d', f.read(8))[0]
            yhi = unpack('<d', f.read(8))[0]
            zlo = unpack('<d', f.read(8))[0]
            zhi = unpack('<d', f.read(8))[0]
            data = np.ones(nx * ny)
            for i in range(nx * ny):
                zdata = unpack('<f', f.read(4))[0]
                if zdata >= 1.701410009187828e+38:
                    data[i] = np.nan
                else:
                    data[i] = zdata

        grd_info = steno3d_surfer.parser.GridInfo(
            ncol=nx,
            nrow=ny,
            xll=xlo,
            yll=ylo,
            xsize=(xhi-xlo)/(nx-1),
            ysize=(yhi-ylo)/(ny-1),
            zmin=zlo,
            zmax=zhi,
            data=data.reshape(nx, ny, order='F')
        )

        return grd_info

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    if v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def load_file(infile_path):
    if not os.path.exists(infile_path):
        sys.stderr.write("File {} does not exist\n".format(infile_path))
        sys.exit(5)

    grd = ssh_grd(infile_path)
    gridinfo = grd.extract()

    #infile_name = os.path.basename(infile_path)
    
    #outfile_data = "{}.data.txt".format(infile_name)
    #np.savetxt(outfile_data, gridinfo.data, delimiter="\t")

    return  gridinfo

def print_file_info(infile_path, gridinfo):
    atts = ["ncol", "nrow", "xll", "yll", "xsize", "ysize", "zmin", "zmax"]

    print("{}:".format(infile_path))
    for att in atts:
        print("{}:\t{}".format(att, getattr(gridinfo, att)))
    # print("data_file:\t{}".format(outfile_data))
    print("")

parser = argparse.ArgumentParser(description='Visualize grid files in argv.')
parser.add_argument('files', nargs='+', help='List of files to animate')
#parser.add_argument('-l', '--log-scale', type=bool, default=False, help='Use log scale. If this option is true, z axis limits are not set.')
parser.add_argument('-l',"--log-scale", type=str2bool, default=False,
                        help="If this option is true, z axis limits are not set.")
parser.add_argument('-p',"--preload", type=str2bool, default=False, help="Files are preloaded before animation.")
parser.add_argument('-i', '--interval', type=int, default=500, help="Interval between animation steps.")
parser.add_argument('-c', '--colormap', default="nipy_spectral", help="For available colormaps see: https://matplotlib.org/examples/color/colormaps_reference.html")
parser.add_argument('--vmin', type=int, default=None, help="Z value minimum")
parser.add_argument('--vmax', type=int, default=None, help="Z value maximum")

args = parser.parse_args()

print("Animated files: ")
print(", ".join(args.files))


preloaded = []

if args.preload:
    print("Please wait preloading...")
    for infile_path in args.files:
        preloaded.append( (infile_path, load_file(infile_path)) )
    gridinfo = preloaded[0][1]
else:
    gridinfo = load_file(args.files[0])

print_file_info(args.files[0], gridinfo)

fig1 = plt.figure()
im = plt.imshow(np.rot90(gridinfo.data), norm=colors.LogNorm() if args.log_scale else None, animated=True, cmap=plt.get_cmap(args.colormap), interpolation="nearest", extent=[gridinfo.xll, gridinfo.xll+gridinfo.xsize*gridinfo.ncol, gridinfo.yll, gridinfo.yll+gridinfo.ysize*gridinfo.nrow])

if not args.log_scale:
    vmin = args.vmin if not args.vmin is None else gridinfo.zmin
    vmax = args.vmax if not args.vmax is None else gridinfo.zmax
    im.set_clim(vmin=vmin,vmax=vmax)

plt.colorbar()

# vmin=0.01, vmax=1

i = 0;
    
def animation_init():
    global i
    i=0;

def animation_update(num):
    global i
    global im
    global args
    global preloaded
    if i+1 >= len(args.files):
        i = 0
    else:
        i += 1

    if args.preload and len(preloaded) > i:
        gridinfo = preloaded[i][1]
    else:
        gridinfo = load_file(args.files[i])

    print_file_info(args.files[i], gridinfo)

    im.set_array(np.rot90(gridinfo.data))
    im.set_extent([gridinfo.xll, gridinfo.xll+gridinfo.xsize*gridinfo.ncol, gridinfo.yll, gridinfo.yll+gridinfo.ysize*gridinfo.nrow])
    if not args.log_scale:
        vmin = args.vmin if not args.vmin is None else gridinfo.zmin
        vmax = args.vmax if not args.vmax is None else gridinfo.zmax
        im.set_clim(vmin=vmin,vmax=vmax)
        
if len(args.files) > 2:
    line_ani = animation.FuncAnimation(fig1, animation_update, len(args.files), animation_init, interval=args.interval)
    #, blit=True, repeat=True
    

plt.show()

