#!/usr/bin/python3
import sys
import numpy as np
import os
import argparse
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.colors as colors
# from numpy import genfromtxt
import csv
from collections import OrderedDict

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    if v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


parser = argparse.ArgumentParser(description='Visualize grid files in argv.')
parser.add_argument('-p', '--poi', help=".poi file - POI positions")
parser.add_argument('-s', '--ssh', help=".poi.ssh file")
# parser.add_argument('-u', '--summary', help=".poi.summary file")
# parser.add_argument('files', nargs='+', help='List of files to animate')
# parser.add_argument('-l', "--log-scale", type=str2bool, default=False,
#                     help="If this option is true, z axis limits are not set.")
# parser.add_argument('-p',"--preload", type=str2bool, default=False, help="Files are preloaded before animation.")
parser.add_argument('-c', '--colormap', default="seismic", #nipy_spectral
                    help="For available colormaps see: https://matplotlib.org/examples/color/colormaps_reference.html")
# parser.add_argument('-d', '--delimiter', default=None, help="Delimiter")
parser.add_argument('-i', '--interval', type=int, default=10, help="Interval between animation steps.")
parser.add_argument('--vmin', type=float, default=None, help="Z value minimum")
parser.add_argument('--vmax', type=float, default=None, help="Z value maximum")

# TERRIBLY INEFFICIENT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

args = parser.parse_args()


def load_poi_positions(poi_file_pathname):
    # poi_pos_map = OrderedDict()
    pois = []
    lons = []
    lats = []
    with open(poi_file_pathname) as poi_file:
        for l in poi_file:
            poi, lon, lat = l.split()
            # print("{} ({}) -> {} ; {} ; {}".format(l,poi, len(poi), lon, lat))
            poi = poi.strip()
            pois.append(poi)
            lat = float(lat)
            lon = float(lon)
            # poi_pos_map[poi] = (lon, lat)
            lons.append(lon)
            lats.append(lat)
            # print("{} | {}".format(len(poi_pos_map),"P0408" in poi_pos_map))

    return pois, lons, lats #, poi_pos_map


def load_ssh_heights(poi_ssh_file_pathname):
    with open(poi_ssh_file_pathname) as poi_ssh_file:
        l = next(poi_ssh_file)
        col_headers = l.split()
        poi_data = OrderedDict()
        for i in range(1, len(col_headers)):
            poi_data[col_headers[i]] = []
        time_data = []

        min_val = float('inf')
        max_val = float('-inf')

        for l in poi_ssh_file:
            row = l.split()
            if len(row) != len(col_headers):
                raise Exception("Number of columns mismatched, row ({}) != header ({})".format(len(row),len(col_headers)))
            time_data.append(row[0])
            for i in range(1, len(row)):
                v = float(row[i])
                poi_data[col_headers[i]].append(v)
                min_val = min(min_val,v)
                max_val = max(max_val,v)

        return time_data, poi_data, min_val, max_val

    return None, None


def smallest_val_diff(uniq_list, init_val=float('inf')):
    min_diff = init_val
    for i in range(1, len(uniq_list)):
        diff = abs(uniq_list[i]-uniq_list[i-1])
        if diff < min_diff:
            min_diff = diff
    return min_diff


def load_heights(time):
    global lons
    global lats
    global pois

    heights = []
    for poi in pois:
        # print("{} ({},{},{},{})".format(poi, len(poi), poi in poi_time_data, "P0408" in poi_time_data, "P0408" == poi))
        if poi in poi_time_data:
            heights.append(poi_time_data[poi][time])
        else:
            heights.append(0)

    assert len(heights) == len(lons) == len(lats) == len(pois)

    # for i in range(0,len(heights)):
    #     print("{} {} {} : {} : {}".format(pois[i], lons[i], lats[i], time_data[time], heights[i]))

    return heights


def draw_hist(heights):
    global lons
    global lats
    global args
    global vmin
    global vmax
    global num_bins_lat
    global num_bins_lon

    # print(vmin, vmax)

    # plt.axis("equal")
    r = plt.hist2d(lons, lats, bins=max(num_bins_lat, num_bins_lon), weights=heights, cmap=args.colormap, vmin=vmin, vmax=vmax)
    plt.colorbar()

    return r


def animation_init():
    global i
    i = 0;


def animation_update(num):
    global i
    global im
    global args
    global preloaded
    global fig1
    global time_data
    global lons
    global lats
    global num_bins_lat
    global num_bins_lon
    global patches

    if i + 1 >= len(preloaded):
        i = 0
    else:
        i += 1

    fig1.clear()

    print(time_data[i])

    draw_hist(preloaded[i])
    # h, xedges, yedges = np.histogram2d(lons, lats, bins=max(num_bins_lat, num_bins_lon), range=None,
    #                                    normed=False, weights=preloaded[i])
    #
    # return patches
    #plt.show()


pois, lons, lats = load_poi_positions(args.poi)
time_data, poi_time_data, min_height, max_height = load_ssh_heights(args.ssh)

min_lon = min(lons)
max_lon = max(lons)
min_lat = min(lats)
max_lat = max(lats)

uniq_lons = list(set(lons))
uniq_lats = list(set(lats))

min_diff_lon = smallest_val_diff(uniq_lons)
min_diff_lat = smallest_val_diff(uniq_lats)

num_bins_lon = int((max_lon-min_lon)/min_diff_lon)/5
num_bins_lat = int((max_lat-min_lat)/min_diff_lat)/5

vmin = min_height if args.vmin is None else args.vmin
vmax = max_height if args.vmax is None else args.vmax

print(num_bins_lat, num_bins_lon)

fig1 = plt.figure()


preloaded = []

print("Preloading...")

for i in range(0, len(time_data)):
    preloaded.append( load_heights(i) )

print("Drawing...")

i = 0
h, xedges, yedges, patches = draw_hist(preloaded[i])

ani = animation.FuncAnimation(fig1, animation_update, len(time_data), animation_init, interval=args.interval)
    # , blit=True, repeat=True


plt.show()

