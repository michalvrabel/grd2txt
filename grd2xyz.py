#!/usr/bin/python3
import sys
import steno3d_surfer
import numpy as np
import os
import argparse
from struct import unpack
import steno3d.parsers
from tqdm import tqdm

class ssh_grd(steno3d_surfer.grd):
    extensions = ('grd', 'ssh', 'sshmax', 'time')

    def parse(self, project=None, verbose=True, as_topo=True):
        super().parse(self, project, verbose, as_topo)

    def _surfer6bin(self, verbose, warnings):
        with open(self.file_name, 'rb') as f:
            if unpack('4s', f.read(4))[0] != b'DSBB':
                raise steno3d.parsers.ParseError(
                    'Invalid file identifier for Surfer 6 binary .grd '
                    'file. First 4 characters must be DSBB.'
                )
            # nx = unpack('<h', f.read(2))[0]
            # ny = unpack('<h', f.read(2))[0]
            nx = unpack('<H', f.read(2))[0]  # changed signed short (<h) to unsigned short (<h)
            ny = unpack('<H', f.read(2))[0]
            xlo = unpack('<d', f.read(8))[0]
            xhi = unpack('<d', f.read(8))[0]
            ylo = unpack('<d', f.read(8))[0]
            yhi = unpack('<d', f.read(8))[0]
            zlo = unpack('<d', f.read(8))[0]
            zhi = unpack('<d', f.read(8))[0]
            data = np.ones(nx * ny)
            for i in range(nx * ny):
                zdata = unpack('<f', f.read(4))[0]
                if zdata >= 1.701410009187828e+38:
                    data[i] = np.nan
                else:
                    data[i] = zdata

        grd_info = steno3d_surfer.parser.GridInfo(
            ncol=nx,
            nrow=ny,
            xll=xlo,
            yll=ylo,
            xsize=(xhi - xlo) / (nx - 1),
            ysize=(yhi - ylo) / (ny - 1),
            zmin=zlo,
            zmax=zhi,
            data=data.reshape(nx, ny, order='F')
        )

        return grd_info

def custom_format_num(format, num):
    if len(format) == 0:
        format = '{:.18e}'
    if format[0] == 'm':
        if num == 0:
            return '0'
        else:
            return format[1:].format(num)
    return format.format(num)

parser = argparse.ArgumentParser(description='Convert grid files to xyz files')
parser.add_argument('files', nargs='+', help='List of files to convert')
parser.add_argument('-s', '--separator', default="\t", help="Field separator")
parser.add_argument('-x', '--x-format', default="{:.18e}", help="x column format (see python format options - https://pyformat.info/)")
parser.add_argument('-y', '--y-format', default="{:.18e}", help="y column format (see python format options - https://pyformat.info/)")
parser.add_argument('-z', '--z-format', default="{:.18e}", help="z column format (see python format options - https://pyformat.info/)")
parser.add_argument('-o', '--data-output', default="stdout", help="destination of data output, possible values: stdout, stderr, file")
parser.add_argument('-i', '--info-output', default="stderr", help="destination of data output, possible values: stdout, stderr, file")
parser.add_argument('-e', '--data-extension', default="txt", help="extension of data files")

args = parser.parse_args()

if args.data_output == "file" and args.info_output == "file":
    print("Converting GRD to XYZ ascii...")
    g = tqdm(args.files)
else:
    g = args.files


for infile_path in g:

    if not os.path.exists(infile_path):
        sys.stderr.write("File {} does not exist\n".format(infile_path))
        sys.exit(5)

    grd = ssh_grd(infile_path)
    gridinfo = grd.extract()

    infile_name = os.path.basename(infile_path)

    outfile_data = "{}.data.xyz.{}".format(infile_name, args.data_extension)
    outfile_info = "{}.info.txt".format(infile_name)

    atts = ["ncol", "nrow", "xll", "yll", "xsize", "ysize", "zmin", "zmax"]

    ofi = sys.stderr
    if args.info_output == "stdout":
        ofi = sys.stdout
    elif args.info_output == "file":
        ofi = open(outfile_info,"w")

    ofi.write("{}:\n".format(infile_path))
    for att in atts:
        ofi.write("{}:\t{}\n".format(att, getattr(gridinfo, att)))

    if args.data_output == "file":
        ofi.write("data_file:\t{}\n".format(outfile_data))

    ofi.write("\n")

    if args.info_output == "file":
        ofi.close()


    ofd = sys.stdout
    if args.data_output == "stderr":
        ofd = sys.stderr
    elif args.data_output == "file":
        ofd = open(outfile_data,"w")

    for j in range(gridinfo.ncol):
        for i in range(gridinfo.nrow):
            ofd.write(args.separator.join([ custom_format_num(args.x_format, j*gridinfo.xsize + gridinfo.xll), custom_format_num(args.y_format, i*gridinfo.ysize + gridinfo.yll), custom_format_num(args.z_format, gridinfo.data[j,i]) ]) + "\n")

    if args.data_output == "file":
        ofd.close()




# vizualizacia

# import matplotlib.pyplot as plt
# plt.imshow(gridinfo.data)
# plt.show()

